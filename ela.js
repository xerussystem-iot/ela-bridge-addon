/*
  Copyright (c) 2010 - 2017, Nordic Semiconductor ASA
  All rights reserved.
  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:
  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.
  2. Redistributions in binary form, except as embedded into a Nordic
     Semiconductor ASA integrated circuit in a product or a software update for
     such product, must reproduce the above copyright notice, this list of
     conditions and the following disclaimer in the documentation and/or other
     materials provided with the distribution.
  3. Neither the name of Nordic Semiconductor ASA nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.
  4. This software, with or without modification, must only be used with a
     Nordic Semiconductor ASA integrated circuit.
  5. Any software provided in binary form under this license must not be reverse
     engineered, decompiled, modified and/or disassembled.
  THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
  OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

'use strict';

const {
  Adapter,
  Device,
  Property
} = require('gateway-addon');

const URL = "http://localhost:8080";

var os = require( 'os' );
var nwkInterfaces = os.networkInterfaces( );
var keys = Object.keys(nwkInterfaces);

const USER = {
  email: 'sriharsha.desai@xerussystems.com',
  name: 'Sriharsha Desai',
  password: 'bridge@123#',
};
 
var Noble = require('@abandonware/noble');
var Request =  require("request");
var sensor_data = require('./sensordb');
const manifest = require('./manifest.json');

var s_data = new sensor_data();

var tempB4 = 'temperature';
var humidB4 = 'humidity';
var self;
var name = 'ElaDevice';
let jwt = '';

//console.log('Reading ELA RHT sensors!');
class ELAProperty extends Property {
  constructor(device, name, propertyDescr) {
    super(device, name, propertyDescr);
    this.onValue = !this.value;
    this.addPropToLog(device, name);
  }

  /**
   * @method setValue
   * @returns a promise which resolves to the updated value.
   *
   * @note it is possible that the updated value doesn't match
   * the value passed in.
   */
  setValue(value) {
 //     return setAndGetProperty(this,value);	 
  }

  async addPropToLog(thing, property) {
    var token = "Bearer " + jwt;
    var url = URL + "/logs";	
    const descr = {
	 descr: {type: 'property', thing: thing.name, property: property,},
	 maxAge: 3600000,
    };
//    console.log('Json value of rawDescr: ' + JSON.stringify(descr));
    
    const res = await Request.post({
         "headers": { "Authorization": token,
		      "content-type": "application/json",
                      "Accept": "application/json" },
         "url": url,
         "body" : JSON.stringify(descr)
    }, (error, response, body) => {
        if(error) {
	   console.log('In error section.');
           return console.dir(error);
        } else {
           //var data = JSON.parse(response.body);
        }
    });

   return;
  }
}


class ElaDevice extends Device {
  constructor(adapter, tag, sensor) {
    super(adapter, `${ElaDevice.name}-${tag.address}`);
    this.tag = tag;
    this['@context'] = 'https://iot.mozilla.org/schemas/';
    this['@type'] = ['TemperatureSensor', 'MultiLevelSensor'];
    this.name = `${sensor.location}-${tag.address}`;
    this.description = 'ELA Relative Humidity and Temperature sensor';
    this.pollinterval = (sensor.Interval || 30) * 1000;

    this.properties.set(
      'temperature',
      new ELAProperty(
        this,
        'temperature',
        {
          label: 'Temperature',
          '@type': 'TemperatureProperty',
          type: 'integer',
          unit: 'degree celsius',
          readOnly: true,
        },
        null
      )
    );

//  if(this.tag.advertisement.localName !== 'P T EN 800329') {
    if(!this.tag.advertisement.localName.toString('utf-8').includes('P T EN')) {
    this.properties.set(
      'humidity',
      new ELAProperty(
        this,
        'humidity',
        {
          label: 'Humidity',
          '@type': 'LevelProperty',
          type: 'integer',
          unit: 'percent',
          minimum: 0,
          maximum: 100,
          readOnly: true,
        },
        null
      )
    );
   }
    this.poll();

  }

/*  async startPolling(interval) {
    await this.updateData(this.tag);
//    Noble.stopScanning();
    this.timer = setInterval(() => {
      this.poll();
    }, interval * 1000);
}
*/

 async poll() {
     await this.updateData(this.tag);
     setTimeout(this.poll.bind(this), this.pollinterval);
  }

   async updateData(devID) {
    return new Promise((resolve, reject) => {
    var serviceData = devID.advertisement.serviceData;
     if (serviceData && serviceData.length) {
       var uuid;
       for (var i in serviceData) {
        uuid = serviceData[i].uuid.toString('utf-8');
  //      console.log('UUID: ', uuid);
         if (uuid === "2a6e") {
	     if (JSON.stringify(serviceData[i].data.readUInt16LE(0)) & 0x8000) {
	        const [ signed ] = new Int16Array([serviceData[i].data.readUInt16LE(0)])
	        this.updateValue('temperature', (signed / 100));
	     } else {
	         this.updateValue('temperature', (serviceData[i].data.readUInt16LE(0) / 100));
             }   
         } else if ((uuid === "2a6f") && (!this.tag.advertisement.localName.includes('P T EN'))) {
	    this.updateValue('humidity', serviceData[i].data.readUInt8(0));
  //          console.log('\t\t Humid: ' + serviceData[i].data.readUInt8(0));
         }
       }
	resolve();
     } else {
	reject();
     }
    });
   }

   async devDiscover() {
    return new Promise((resolve, reject) => {
      Noble.on('discover', function(peripheral) {
	   if (peripheral.id === 'f78c077ecab2' || peripheral.id === 'e0514538b920' || peripheral.advertisement.localName === 'P T EN 800329' ) {
//	       console.log(`Discovered new ElaDevice ${peripheral.id}`);
	       resolve(peripheral);
           }
      });
    });
  }

  updateValue(name, value) {
//    console.log(`Set ${name} to ${value}`);
    const property = this.properties.get(name);
    property.setCachedValue(value);
    this.notifyPropertyChanged(property);
  }

  async createThing(descr) {
//  console.log('createThing: ' + JSON.stringify(descr)); 
    var token = "Bearer " + jwt;
    var url = URL + "/things";
    const res = await Request.post({
         "headers": { "Authorization": token  ,
		      "content-type": "application/json",
                      "Accept": "application/json" },
         "url": url,
         "body" : JSON.stringify(descr)
    }, (error, response, body) => {
        if(error) {
	   console.log('In error section.');
           return console.dir(error);
        } else {
//	   console.log('Body: ' + data.jwt);
        }
    });
   return;
  }

}

class ElaAdapter extends Adapter {
  constructor(addonManager, config, db) {
    super(addonManager, ElaAdapter.name, manifest.name);
    addonManager.addAdapter(this);
    s_data.open();
    this.ip = 0;
    this.config = config;
    this.db = db;
    this.knownDevices = new Set();
    this.sensorData = new Set();
    this.sensorAdd = [];
    self = this;    
    for(var index = 0; index <keys.length; index++) {
       if ( keys[index] != 'lo') {
           this.ip = nwkInterfaces[keys[index]][0]['address'];
       }
    }
//    console.log('Json data of sensor: ' + JSON.stringify(this.sensorData));
    this.DiscoverandAddDev();
  }

 async DiscoverandAddDev() {
  return new Promise(function(resolve, reject) {
   setTimeout(function() {
      resolve(s_data.loadSensorsDb());
    }, 1000);
  }).then(function(value) {
     console.log('Addon code:', value.length);
//     self.sensorData = value;
//     console.log('Json data of sensor: ' + JSON.stringify(self.sensorData));
   if(value.length != 0) {
      for(let val of value) {
        var sobj = new Object();
        sobj.address = val['Sensor_MAC'].trim();
        sobj.name = val['Sensor_Name'].trim();
        self.sensorAdd.push(sobj);
        self.sensorData[val['Sensor_MAC']] = val;
//	console.log('Mac value: ' + JSON.stringify(self.sensorData[val['Sensor_MAC']]));
      }
        const peripheral = self.getjwtToken();
    }
  });
 }
 
CheckUpdate() {
    var timer = setInterval(() => {
        self.UpdateDev();
    },20 * 1000);

}

comparer(diffSet){
  return function(current){
    return diffSet.filter(function(diff){
      return diff.address == current.address;
    }).length == 0;
  }
}

diff(set1,set2){
  var InA1 = set1.filter(self.comparer(set2));
  var InA2 = set2.filter(self.comparer(set1));
  return InA1;
}

async UpdateDev() {
  return new Promise(function(resolve, reject) {
   setTimeout(function() {
      resolve(s_data.loadSensorsDb());
    }, 1000);
  }).then(function(value) {
     console.log('Sunil Code: ', value.length);
//     self.sensorData = value;
//     console.log('Json data of sensor: ' + JSON.stringify(self.sensorData));
   
   if(value.length != 0) {
      var difference = self.diff(self.sensorAdd,value);
   console.log('Difference value: ', difference);
      if(difference.length > 0) {
         for(let val of value) {
            var sobj = new Object();
            sobj.address = val['Sensor_MAC'].trim();
            sobj.name = val['Sensor_Name'].trim();
            self.sensorAdd.push(sobj);
            self.sensorData[val['Sensor_MAC']] = val;
//	    console.log('Mac value: ' + JSON.stringify(self.sensorData[val['Sensor_MAC']]));
         }
         self.discover(difference);
      }  
    }
  });
 }
  async getjwtToken() {
    var url = URL + "/login";
//    console.log('user login json value: ' + JSON.stringify(USER));
    const res = await Request.post({
         "headers": { "content-type": "application/json",
                      "Accept": "application/json" },
         "url" : url,
         "body" : JSON.stringify(USER)
    }, (error, response, body) => {
        if(error) {
	   console.log('In error section.');
           return console.dir(error);
        } else {
           var data = JSON.parse(response.body);	
	   jwt = data.jwt;
          Noble.startScanning([], true);
          self.discover(self.sensorAdd);
          self.CheckUpdate();
        }
    });
   	
   return;  

  }
       /*pass list of sensors*/
   async discover(sensorAdd) {
    return new Promise((resolve, reject) => {
      Noble.on('discover', function(peripheral) {
      for(let add of sensorAdd) {
       if ((add.address === peripheral.address) && ((add.name.includes('P T EN')) || (add.name.includes('P RHT')))) {
             const knownDevice = self.knownDevices.has(peripheral.address);
              if (!knownDevice) {
//	       console.log('Discovered new ElaDevice: ' + JSON.stringify(self.sensorData[add]));
		var val = self.sensorData[add.address];
//		console.log('Json value: '  + val['Sensor_Name']);
		console.log('Device IP: ' + self.ip);
		var sensor = new Object();
		   sensor.name = val['Sensor_Name'];
		   sensor.mac = val['Sensor_MAC'];
		   sensor.Interval = val['LogInterval'];
		   sensor.location = val['Location'].trim();
		   sensor.host = val['Bridge_Name'];
		   sensor.id = name + '-' + val['Sensor_MAC'];
		   sensor.ip = self.ip;

		   if(self.config.sensor.length === 0) {
	   		self.config.sensor.push(sensor);
			self.db.saveConfig(self.config);
//			console.log('Config: ' + self.config.sensor[0]["id"]);
		   }else {
    			  for(var index = 0; index < self.config.sensor.length; index++ ) {
                            var id = self.config.sensor[index]["id"];
			    if( id === sensor.id)
				self.duplicate = 1;
 			  }
			if( !self.duplicate ) {
			    self.config.sensor.push(sensor);
                            self.db.saveConfig(self.config);
			} 
 		   }

		console.log('Config param: ' + JSON.stringify(self.config.sensor));
		const device = new ElaDevice(self, peripheral, sensor/*self.pollInterval*/);
		device.createThing(device.asThing());
		self.knownDevices.add(peripheral.address);
		self.handleDeviceAdded(device);
		resolve();
	      }
           }
	 }
      });
    });
  }
}


module.exports = ElaAdapter;
