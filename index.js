
'use strict';

// eslint-disable-next-line max-len
const {Database} = require('gateway-addon');
const manifest = require('./manifest.json');
const ElaAdapter = require('./ela');

//module.exports =
    // eslint-disable-next-line max-len
//    (addonManager, manifest) => new ElaAdapter(addonManager, manifest);

module.exports = (addonManager, _, errorCallback) => {
  const db = new Database(manifest.id);
  db.open().then(() => {
    return db.loadConfig();
  }).then((config) => {
    /*if (!config.sensor || config.sensor.length === 0) {	    
      errorCallback(manifest.id, 'sensor\'s not configured.');
      return;
    }*/	  
    new ElaAdapter(addonManager, config ,db);
  }).catch((e) => {
    console.log("sunil in error");	  
    errorCallback(manifest.id, e);
  });
};

